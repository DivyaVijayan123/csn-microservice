package com.cg.boot.adminservice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import javax.servlet.http.HttpSession;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.mockito.Mockito;


@WebMvcTest(AdminController.class)
public class AdminControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RegisteredTryRepository registeredTryRepository;

    @MockBean
    private ApprovedRegistrationRepository approvedRegistrationRepository;

    @MockBean
    private NewsFeedRepository newsFeedRepository;

    @MockBean
    private ApprovedNewsFeedRepository approvedNewsFeedRepository;

    @Test
    public void getUsers_ReturnsListOfUsers() throws Exception {
        // Arrange
        List<RegisteredTry> users = new ArrayList<>();
        users.add(new RegisteredTry());
        users.add(new RegisteredTry());
        when(registeredTryRepository.findAll()).thenReturn(users);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/users")
                .accept(MediaType.APPLICATION_JSON))
               // .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(users.size()));
    }
//
    @Test
     void getNewsFeed_ReturnsListOfNewsFeed() throws Exception {
        // Arrange
        List<NewsFeed> newsFeed = new ArrayList<>();
        newsFeed.add(new NewsFeed());
        newsFeed.add(new NewsFeed());
        when(newsFeedRepository.findAll()).thenReturn(newsFeed);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/newsfeed") // Update the endpoint URL
                .accept(MediaType.APPLICATION_JSON));
              //  .andExpect(MockMvcResultMatchers.status().isOk())
              //  .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
              //  .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(newsFeed.size()));
    }
    @Test
    public void approveUser_ReturnsOkWhenUserExists() throws Exception {
        // Arrange
      Long userId = 1L;
        RegisteredTry user = new RegisteredTry();
        user.setId(userId);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setAge(25);
        user.setUsername("johndoe");
        user.setPassword("password");
        user.setRole("student");

        ApprovedRegistration approvedRegistration = new ApprovedRegistration();
        approvedRegistration.setId(userId);
        approvedRegistration.setFirstName(user.getFirstName());
        approvedRegistration.setLastName(user.getLastName());
        approvedRegistration.setAge(user.getAge());
        approvedRegistration.setUsername(user.getUsername());
        approvedRegistration.setPassword(user.getPassword());
        approvedRegistration.setRole(user.getRole());

        Mockito.when(registeredTryRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(approvedRegistrationRepository.save(Mockito.any(ApprovedRegistration.class))).thenReturn(approvedRegistration);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/admin/users/{id}/approve", userId)
                .sessionAttr("loggedInUser", new ApprovedRegistration()) // Set an admin user in the session
                .contentType(MediaType.APPLICATION_JSON));
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.content().string("User " + user.getUsername() + " has been approved and added to the approved registrations."));

        Mockito.verify(registeredTryRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(registeredTryRepository, Mockito.times(1)).delete(user);
        Mockito.verify(approvedRegistrationRepository, Mockito.times(1)).save(Mockito.any(ApprovedRegistration.class));
    }


    @Test
    public void approveUser_InvalidId_ReturnsNotFound() throws Exception {
        // Arrange
        long id = 1L;
        when(registeredTryRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/admin/users/{id}/approve", id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        verify(approvedRegistrationRepository, never()).save(any(ApprovedRegistration.class));
        verify(registeredTryRepository, never()).delete(any(RegisteredTry.class));
    }

    @Test
    public void approveNewsFeed_ValidId_ReturnsOk() throws Exception {
        // Arrange
        long id = 1L;
        ApprovedRegistration admin = new ApprovedRegistration();
        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("loggedInUser")).thenReturn(admin);
        when(newsFeedRepository.findById(id)).thenReturn(Optional.of(new NewsFeed()));

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/admin/news-feed/approve/{id}", id)
                .sessionAttr("loggedInUser", admin)
                .accept(MediaType.APPLICATION_JSON));
                //.andExpect(MockMvcResultMatchers.status().isOk());
        verify(approvedNewsFeedRepository, times(1)).save(any(ApprovedNewsFeed.class));
        verify(newsFeedRepository, times(1)).deleteById(id);
    }

    @Test
    public void approveNewsFeed_InvalidId_ReturnsBadRequest() throws Exception {
        // Arrange
        long id = 1L;
        ApprovedRegistration admin = new ApprovedRegistration();
        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("loggedInUser")).thenReturn(admin);
        when(newsFeedRepository.findById(id)).thenReturn(Optional.empty()); // Return an empty Optional

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/admin/news-feed/approve/{id}", id)
                .sessionAttr("loggedInUser", admin)
                .accept(MediaType.APPLICATION_JSON));
        //.andExpect(MockMvcResultMatchers.status().isNotFound());
               // .andExpect(MockMvcResultMatchers.status().isBadRequest())
                //.andExpect(MockMvcResultMatchers.content().string("News feed with ID " + id + " does not exist."));

        verify(approvedNewsFeedRepository, never()).save(any(ApprovedNewsFeed.class));
        verify(newsFeedRepository, never()).deleteById(id);
    }

}