package com.cg.boot.adminservice;

import java.util.Optional;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApprovedRegistrationRepository extends JpaRepository<ApprovedRegistration, Long> {
    ApprovedRegistration findByUsernameAndPasswordAndRole(String username, String password, String Role);

	

	Optional<ApprovedRegistration> findByUsername(String username);
}