package com.cg.boot.staffservice;

import java.time.LocalDate;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/staff")
public class StaffController {
    
    @Autowired
    private StudentRepository studentRepository;
    
    @Autowired
    private EventRepository eventRepository;

    private static final String LOGIN_MESSAGE = "loggedInUser";
    @SuppressWarnings("unused")
	@GetMapping("/students")
    public ResponseEntity<?> getStudents(HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);
        
        List<Student> students = studentRepository.findAll();
        
        return ResponseEntity.ok(students);
    }
    
    @SuppressWarnings("unused")
	@PostMapping("/events")
    public ResponseEntity<?> addEvent(@RequestBody EventRequest eventRequest, HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);
        
        Event event = new Event();
        event.setEvent(eventRequest.getEvent());
        event.setDateOfEvent(eventRequest.getDateOfEvent());
        event.setPeriod(eventRequest.getPeriod());
        
        eventRepository.save(event);
        
        return ResponseEntity.ok(event);
    }
    @SuppressWarnings("unused")
	@PutMapping("/events/{id}")
    public ResponseEntity<?> updateEvent(@PathVariable Long id, @RequestBody EventRequest eventRequest, HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);
        Optional<Event> optionalEvent = eventRepository.findById(id);
        
        if (!optionalEvent.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        
        Event event = optionalEvent.get();
        
        event.setEvent(eventRequest.getEvent());
        event.setDateOfEvent(eventRequest.getDateOfEvent());
        event.setPeriod(eventRequest.getPeriod());
        
        eventRepository.save(event);
        return ResponseEntity.ok(event);
    }
    @SuppressWarnings("unused")
	@DeleteMapping("/events/{id}")
    public ResponseEntity<Event> deleteEvent(@PathVariable("id") Long id, HttpSession session) {
        ApprovedRegistration user = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);
        Event event = eventRepository.findById(id).orElse(null);

        if (event == null) {
            return ResponseEntity.notFound().build();
        }

        eventRepository.delete(event);
        return ResponseEntity.ok(event);
    }
    @GetMapping("/events")
    public ResponseEntity<List<Event>> getAllEvents() {
        List<Event> events = eventRepository.findAll();
        return ResponseEntity.ok(events);
    }
    
    static class EventRequest {
    	@NotBlank @Size(min=3,max=10)
        private String event;
    	
        private LocalDate dateOfEvent;
        
        @Min(1)
        private int period;

        public String getEvent() {
            return event;
        }

        public void setEvent(String event) {
            this.event = event;
        }

        public LocalDate getDateOfEvent() {
            return dateOfEvent;
        }

        public void setDateOfEvent(LocalDate dateOfEvent) {
            this.dateOfEvent = dateOfEvent;
        }

        public int getPeriod() {
            return period;
        }

        public void setPeriod(int period) {
            this.period = period;
        }
    }
}

