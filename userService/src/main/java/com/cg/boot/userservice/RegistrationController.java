package com.cg.boot.userservice;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class RegistrationController {
 
    @Autowired
    private RegisteredTryRepository registeredTryRepository;
 
    @PostMapping("/register")
    public void registerUser(@RequestBody RegisteredTry registeredTry) {
        registeredTryRepository.save(registeredTry);
    }
}
