package com.cg.boot.userservice;
import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface RegisteredTryRepository extends JpaRepository<RegisteredTry, Long> {
 
    RegisteredTry findByUsernameAndPasswordAndRole(String username, String password, String role);

	Optional<RegisteredTry> findByUsername(String username);
 
}