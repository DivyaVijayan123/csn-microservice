package com.cg.boot.userservice;

import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<?> getUsers();

    ResponseEntity<?> getNewsFeed();

    ResponseEntity<?> approveUser(Long id);

    ResponseEntity<?> approveNewsFeed(Long id);


    ResponseEntity<?> updatePost(Long id);

    ResponseEntity<?> deletePost(Long id);

    ResponseEntity<?> getAllPosts();

    ResponseEntity<?> getStudents();
    
    ResponseEntity<?> getGroupMessages(String groupIdentifier);
    
    ResponseEntity<?> deleteGroupMessage(String groupId);
    
    ResponseEntity<?> updateGroupMessage(String groupId);

    ResponseEntity<?> deleteMessage(Long messageId);

    ResponseEntity<?> updateMessage(Long messageId);

    ResponseEntity<?> getMessagesForLoggedInUser();

    ResponseEntity<?> deleteEvent(Long id);

    ResponseEntity<?> getAllEvents();

    ResponseEntity<?> updateEvent(Long id);

    ResponseEntity<?> updateArticle(Long id);

    ResponseEntity<?> deleteArticle(Long id);

    // Add other methods for communication with the user-service microservice
}
