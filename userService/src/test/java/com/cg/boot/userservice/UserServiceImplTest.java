package com.cg.boot.userservice;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


class UserServiceImplTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testDeleteArticle() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://student-service/student/article/{id}"), eq(HttpMethod.DELETE), isNull(), 
                eq(ResponseEntity.class), eq(1L))).thenReturn((ResponseEntity<ResponseEntity>) responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.deleteArticle(1L);

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://student-service/student/article/{id}"), eq(HttpMethod.DELETE), isNull(),
                eq(ResponseEntity.class), eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testUpdateEvent() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://staff-service/update-event/{id}"), eq(HttpMethod.PUT), isNull(), 
                any(ParameterizedTypeReference.class), eq(1L))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.updateEvent(1L);

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://staff-service/update-event/{id}"), eq(HttpMethod.PUT), isNull(),
                any(ParameterizedTypeReference.class), eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testDeleteEvent() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://staff-service/staff/events/{id}"), eq(HttpMethod.DELETE), isNull(), 
                eq(ResponseEntity.class), eq(1L))).thenReturn((ResponseEntity<ResponseEntity>) responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.deleteEvent(1L);

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://staff-service/staff/events/{id}"), eq(HttpMethod.DELETE), isNull(),
                eq(ResponseEntity.class), eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testGetAllEvents() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://staff-service/events"), eq(HttpMethod.GET), isNull(), 
                any(ParameterizedTypeReference.class))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.getAllEvents();

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://staff-service/events"), eq(HttpMethod.GET), isNull(),
                any(ParameterizedTypeReference.class));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }
    @Test
    void testGetMessagesForLoggedInUser() {
        // Mocking the response entity
        ResponseEntity<ResponseEntity> responseEntity = ResponseEntity.ok().build();
        
        // Creating matchers for the arguments
        String url = "http://chat-service/chat/chat/messages"; // Corrected URL
        HttpMethod method = HttpMethod.GET;
        HttpEntity<?> requestEntity = null;
        Class<ResponseEntity> responseType = ResponseEntity.class;
        
        // Using matchers for each argument in the `thenReturn()` method
        when(restTemplate.exchange(eq(url), eq(method), eq(requestEntity), eq(responseType)))
                .thenReturn(responseEntity);
        
        // Calling the service method
        ResponseEntity<?> response = userService.getMessagesForLoggedInUser();
        
        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq(url), eq(method), eq(requestEntity), eq(responseType));
        
        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }


    @Test
    void testUpdateMessage() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://chat-service/chat/update/{groupId}"), eq(HttpMethod.PUT), isNull(),
                any(ParameterizedTypeReference.class), eq(1L))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.updateMessage(1L);

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://chat-service/chat/update/{groupId}"), eq(HttpMethod.PUT), isNull(),
                any(ParameterizedTypeReference.class), eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }


    @Test
    void testDeleteMessage() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        when(restTemplate.exchange(eq("http://chat-service/chat/chat/delete/{messageId}"), eq(HttpMethod.DELETE), isNull(),
                eq(ResponseEntity.class), eq(1L))).thenReturn((ResponseEntity<ResponseEntity>) responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.deleteMessage(1L);

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://chat-service/chat/chat/delete/{messageId}"), eq(HttpMethod.DELETE), isNull(),
                eq(ResponseEntity.class), eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }


    @Test
    void testUpdateGroupMessage() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://chat-service/chat/update/{groupId}"), eq(HttpMethod.PUT), isNull(), 
                any(ParameterizedTypeReference.class), eq("group123"))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.updateGroupMessage("group123");

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://chat-service/chat/update/{groupId}"), eq(HttpMethod.PUT), isNull(),
                any(ParameterizedTypeReference.class), eq("group123"));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }


    @Test
    void testDeleteGroupMessage() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://chat-service/chat/chat/delete-group/{groupId}"), eq(HttpMethod.DELETE), isNull(), 
                eq(ResponseEntity.class), eq("group123"))).thenReturn((ResponseEntity<ResponseEntity>) responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.deleteGroupMessage("group123");

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://chat-service/chat/chat/delete-group/{groupId}"), eq(HttpMethod.DELETE), isNull(),
                eq(ResponseEntity.class), eq("group123"));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testGetGroupMessages() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        
        when(restTemplate.exchange(
            eq("http://chat-service/chat/chat/group-messages?groupIdentifier={groupIdentifier}"), 
            eq(HttpMethod.GET), 
            isNull(), 
            eq(ResponseEntity.class), 
            eq("group123")
        )).thenReturn((ResponseEntity<ResponseEntity>) responseEntity);
        
        // Calling the service method
        ResponseEntity<?> response = userService.getGroupMessages("group123");
        
        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(
            eq("http://chat-service/chat/chat/group-messages?groupIdentifier={groupIdentifier}"), 
            eq(HttpMethod.GET), 
            isNull(), 
            eq(ResponseEntity.class), 
            eq("group123")
        );
        
        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }



    @Test
    void testUpdatePost() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://placement-service/students/update-post/{id}"), eq(HttpMethod.PUT), isNull(), 
                any(ParameterizedTypeReference.class), eq(1L))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.updatePost(1L);

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://placement-service/students/update-post/{id}"), eq(HttpMethod.PUT), isNull(),
                any(ParameterizedTypeReference.class), eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testDeletePost() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://placement-service/students/delete-post/{id}"), eq(HttpMethod.DELETE), isNull(), 
                any(ParameterizedTypeReference.class), eq(1L))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.deletePost(1L);

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://placement-service/students/delete-post/{id}"), eq(HttpMethod.DELETE), isNull(),
                any(ParameterizedTypeReference.class), eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testGetAllPosts() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://placement-service/students/posts"), eq(HttpMethod.GET), isNull(), 
                any(ParameterizedTypeReference.class))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.getAllPosts();

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://placement-service/students/posts"), eq(HttpMethod.GET), isNull(),
                any(ParameterizedTypeReference.class));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testGetStudents() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://placement-service/students/students"), eq(HttpMethod.GET), isNull(), 
                any(ParameterizedTypeReference.class))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.getStudents();

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://placement-service/students/students"), eq(HttpMethod.GET), isNull(),
                any(ParameterizedTypeReference.class));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testGetUsers() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://user-service/api/registrations"), eq(HttpMethod.GET), isNull(), 
                any(ParameterizedTypeReference.class))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.getUsers();

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://user-service/api/registrations"), eq(HttpMethod.GET), isNull(),
                any(ParameterizedTypeReference.class));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testGetNewsFeed() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(eq("http://user-service/api/newsfeed"), eq(HttpMethod.GET), isNull(), 
                any(ParameterizedTypeReference.class))).thenReturn(responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.getNewsFeed();

        // Verifying the restTemplate.exchange method is called
        verify(restTemplate).exchange(eq("http://user-service/api/newsfeed"), eq(HttpMethod.GET), isNull(),
                any(ParameterizedTypeReference.class));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testApproveUser() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.postForEntity(eq("http://user-service/api/users/{id}/approve"), isNull(), eq(ResponseEntity.class), 
                eq(1L))).thenReturn((ResponseEntity<ResponseEntity>) responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.approveUser(1L);

        // Verifying the restTemplate.postForEntity method is called
        verify(restTemplate).postForEntity(eq("http://user-service/api/users/{id}/approve"), isNull(), eq(ResponseEntity.class), 
                eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }

    @Test
    void testApproveNewsFeed() {
        // Mocking the response entity
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.postForEntity(eq("http://user-service/api/news-feed/approve/{id}"), isNull(), eq(ResponseEntity.class), 
                eq(1L))).thenReturn((ResponseEntity<ResponseEntity>) responseEntity);

        // Calling the service method
        ResponseEntity<?> response = userService.approveNewsFeed(1L);

        // Verifying the restTemplate.postForEntity method is called
        verify(restTemplate).postForEntity(eq("http://user-service/api/news-feed/approve/{id}"), isNull(), eq(ResponseEntity.class), 
                eq(1L));

        // Verifying the response
        assertThat(response).isEqualTo(responseEntity);
    }
    @Test
    void testUpdateArticle() {
        // Mock the response from the restTemplate.exchange() method
        ResponseEntity<?> responseEntity = ResponseEntity.ok().build();
      when(restTemplate.exchange(eq("http://student-service/update-article/{id}"), eq(HttpMethod.PUT), isNull(), 
                any(ParameterizedTypeReference.class), eq(1L))).thenReturn(responseEntity);


        // Perform the test
        ResponseEntity<?> response = userService.updateArticle(1L);

        // Verify the restTemplate.exchange() method is called
        verify(restTemplate).exchange(eq("http://student-service/update-article/{id}"), eq(HttpMethod.PUT), isNull(),
                any(ParameterizedTypeReference.class), eq(1L));


        // Verify the response
        assertThat(response).isEqualTo(responseEntity);
        // Perform further assertions as needed
    }

  

}
