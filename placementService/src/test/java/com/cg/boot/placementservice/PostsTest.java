package com.cg.boot.placementservice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class PostsTest {

    @Test
    public void testGettersAndSetters() {
        Long id = 1L;
        String title = "Test Title";
        String content = "Test Content";
        Date postedDate = new Date();
        String username = "Test User";

        Posts post = new Posts();

        post.setId(id);
        post.setTitle(title);
        post.setContent(content);
        post.setPostedDate(postedDate);
        post.setUsername(username);

        Assertions.assertEquals(id, post.getId());
        Assertions.assertEquals(title, post.getTitle());
        Assertions.assertEquals(content, post.getContent());
        Assertions.assertEquals(postedDate, post.getPostedDate());
        Assertions.assertEquals(username, post.getUsername());
    }
}
