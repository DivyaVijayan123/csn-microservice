package com.cg.boot.placementservice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StudentTest {

    @Test
    public void testGettersAndSetters() {
        long id = 1L;
        String name = "John Doe";
        String department = "Computer Science";
        int backlogs = 2;
        double percentage = 85.5;

        Student student = new Student();

        student.setId(id);
        student.setName(name);
        student.setDepartment(department);
        student.setBacklogs(backlogs);
        student.setPercentage(percentage);

        Assertions.assertEquals(id, student.getId());
        Assertions.assertEquals(name, student.getName());
        Assertions.assertEquals(department, student.getDepartment());
        Assertions.assertEquals(backlogs, student.getBacklogs());
        Assertions.assertEquals(percentage, student.getPercentage(), 0.01);
    }
}
