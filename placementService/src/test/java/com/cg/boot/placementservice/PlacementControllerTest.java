package com.cg.boot.placementservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.net.URI;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


class PlacementControllerTest {
@Autowired
    private PlacementController placementController;
    @MockBean
    private StudentRepository studentRepository;
    @MockBean
    private PostsRepository postsRepository;
    @MockBean
    private RestTemplate restTemplate;
    private HttpSession session;

    @BeforeEach
    void setUp() {
        studentRepository = mock(StudentRepository.class);
        postsRepository = mock(PostsRepository.class);
        restTemplate = mock(RestTemplate.class);
        session = mock(HttpSession.class);
        placementController = new PlacementController(postsRepository);
        placementController.setStudentRepository(studentRepository);
        placementController.setPostsRepository(postsRepository);
        placementController.setRestTemplate(restTemplate);
    }

    @Test
    void testAddStudent() {
        // Mock the request body
        PlacementController.StudentDetails studentDetails = new PlacementController.StudentDetails();
        studentDetails.setId(1L);
        studentDetails.setName("John Doe");
        studentDetails.setDepartment("Computer Science");
        studentDetails.setBacklogs(0);
        studentDetails.setPercentage(90.5);

        // Mock the session
        when(session.getAttribute("loggedInUser")).thenReturn(null);

        // Mock the student repository
        when(studentRepository.save(any(Student.class))).thenReturn(new Student());

        // Perform the addStudent() method
        ResponseEntity<String> response = placementController.addStudent(studentDetails, session);

        // Verify the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("Student added successfully!");
    }

    @Test
    void testAddPost() {
        // Mock the request body
        PlacementController.PostDetails postDetails = new PlacementController.PostDetails();
        postDetails.setId(1L);
        postDetails.setTitle("Test Post");
        postDetails.setContent("This is a test post");

        // Mock the session
        when(session.getAttribute("loggedInUser")).thenReturn(null);

        // Mock the posts repository
        when(postsRepository.save(any(Posts.class))).thenReturn(new Posts());

        // Create an instance of the PlacementController
        placementController = new PlacementController(postsRepository);

        // Perform the addPost() method
        ResponseEntity<Posts> response = placementController.addPost(postDetails, session);

        // Verify the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    void testUpdatePost() {
        // Mock the request body
        PlacementController.PostDetails postDetails = new PlacementController.PostDetails();
        postDetails.setTitle("Updated Post");
        postDetails.setContent("This post has been updated");

        // Mock the session
        when(session.getAttribute("loggedInUser")).thenReturn(null);

        // Mock the post from the posts repository
        Posts post = new Posts();
        post.setId(1L);
        post.setTitle("Old Post");
        post.setContent("This is an old post");
        when(postsRepository.findById(1L)).thenReturn(java.util.Optional.of(post));
        when(postsRepository.save(any(Posts.class))).thenReturn(post);

        // Perform the updatePost() method
        ResponseEntity<Posts> response = placementController.updatePost(1L, postDetails, session);

        // Verify the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getTitle()).isEqualTo("Updated Post");
        assertThat(response.getBody().getContent()).isEqualTo("This post has been updated");
    }

    @Test
    void testDeletePost() {
        // Mock the session
        when(session.getAttribute("loggedInUser")).thenReturn(null);

        // Mock the post from the posts repository
        Posts post = new Posts();
        post.setId(1L);
        post.setTitle("Test Post");
        post.setContent("This is a test post");

        // Mock the postsRepository behavior
        when(postsRepository.findById(1L)).thenReturn(Optional.of(post));
        when(postsRepository.findById(2L)).thenReturn(Optional.empty()); // Mocking the case when post is not found

        // Test when the post exists
        ResponseEntity<Posts> responseExisting = placementController.deletePost(1L, session);
        assertThat(responseExisting.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseExisting.getBody()).isNotNull();
        assertThat(responseExisting.getBody().getId()).isEqualTo(1L);

        // Test when the post does not exist
        ResponseEntity<Posts> responseNotExisting = placementController.deletePost(2L, session);
        assertThat(responseNotExisting.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(responseNotExisting.getBody()).isNull();
    }

    @Test
    void testGetAllPosts() {
        // Mock the posts from the posts repository
        List<Posts> postsList = new ArrayList<>();
        postsList.add(new Posts());
        postsList.add(new Posts());
        when(postsRepository.findAll()).thenReturn(postsList);

        // Perform the getAllPosts() method
        ResponseEntity<List<Posts>> response = placementController.getAllPosts();

        // Verify the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().size()).isEqualTo(2);
    }

    @Test
    void testGetStudents() {
        // Mock the session
        when(session.getAttribute("loggedInUser")).thenReturn(null);

        // Mock the students from the student repository
        List<Student> studentsList = new ArrayList<>();
        studentsList.add(new Student());
        studentsList.add(new Student());
        when(studentRepository.findAll()).thenReturn(studentsList);

        // Perform the getStudents() method
        ResponseEntity<?> response = placementController.getStudents(session);

        // Verify the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).isInstanceOf(List.class);
        List<?> students = (List<?>) response.getBody();
        assertThat(students.size()).isEqualTo(2);
    }
 @Test
    void testGetUsernameFromUserService() {
        // Mock the response from the restTemplate.exchange() method
        String expectedUsername = "test_user";
        ResponseEntity<String> mockResponse = ResponseEntity.ok(expectedUsername);
        when(restTemplate.exchange(
                eq("http://user-service/api/username"),
                eq(HttpMethod.GET),
                eq(null),
                eq(String.class)
        )).thenReturn(mockResponse);

        // Perform the getUsernameFromUserService() method
        String username = placementController.getUsernameFromUserService();

        // Verify the response
        assertThat(username).isNotNull();
        assertThat(username).isEqualTo(expectedUsername);
    }

//

//    @Test
//    void testAddPost() {
//        // Mock the session
//        HttpSession session = mock(HttpSession.class);
//        when(session.getAttribute("loggedInUser")).thenReturn(mock(ApprovedRegistration.class));
//
//        // Mock the post details
//        PlacementController.PostDetails postDetails = new PlacementController.PostDetails();
//        postDetails.setId(1L);
//        postDetails.setTitle("Test Post");
//        postDetails.setContent("This is a test post");
//
//        // Mock the response from the getUsernameFromUserService() method
//        when(placementController.getUsernameFromUserService()).thenReturn("test_user");
//
//        // Perform the addPost() method
//        ResponseEntity<Posts> response = placementController.addPost(postDetails, session);
//
//        // Verify the response
//        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
//        assertThat(response.getBody()).isNotNull();
//        // Add more assertions as needed
//    }

}
