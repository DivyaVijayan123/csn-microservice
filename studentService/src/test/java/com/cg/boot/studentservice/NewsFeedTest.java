package com.cg.boot.studentservice;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@WebMvcTest(NewsFeed.class)
 class NewsFeedTest {

	@Test
	void testNewsFeedCreation() {
	    // Create a new NewsFeed object
	    NewsFeed newsFeed = new NewsFeed();
	    newsFeed.setTitle("Test Title");
	    newsFeed.setContent("Test Content");
	    newsFeed.setPostedDate(new Date());
	    newsFeed.setStudentId(1L);
	    newsFeed.setStudentName("John Doe");
	
	    // Verify the values of the NewsFeed object
	   // assertNotNull(newsFeed.getId()); // Remove this line
	    assertEquals("Test Title", newsFeed.getTitle());
	    assertEquals("Test Content", newsFeed.getContent());
	    assertNotNull(newsFeed.getPostedDate());
	    assertEquals(1L, newsFeed.getStudentId());
	    assertEquals("John Doe", newsFeed.getStudentName());
	}
	
}

