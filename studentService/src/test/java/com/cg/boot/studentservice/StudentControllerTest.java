package com.cg.boot.studentservice;

import org.junit.jupiter.api.Test;

import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpSession;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@WebMvcTest(StudentController.class)
 class StudentControllerTest {

    @MockBean
    private NewsFeedRepository newsFeedRepository;
    @Autowired
    private StudentController studentController;
    @MockBean
    private ApprovedNewsFeedRepository approvedNewsFeedRepository;

    @Mock
    private HttpSession session;

    @Test
    void testAddArticle() {
        String title = "Test Article";
        String content = "Test Content";

        ResponseEntity<String> response = studentController.addArticle(title, content, session);

        verify(newsFeedRepository, times(1)).save(any(NewsFeed.class));
        assertEquals(ResponseEntity.ok("Article added successfully."), response);
    }

    @Test
    void testUpdateArticle() {
        Long id = 1L;
        String title = "Updated Article";
        String content = "Updated Content";

        ApprovedNewsFeed newsFeed = new ApprovedNewsFeed();
        newsFeed.setId(id);
        newsFeed.setTitle("Old Title");
        newsFeed.setContent("Old Content");

        when(approvedNewsFeedRepository.findById(id)).thenReturn(Optional.of(newsFeed));

        ResponseEntity<String> response = studentController.updateArticle(id, title, content, session);

        verify(approvedNewsFeedRepository, times(1)).save(newsFeed);
        assertEquals(ResponseEntity.ok("Article updated successfully."), response);
        assertEquals(title, newsFeed.getTitle());
        assertEquals(content, newsFeed.getContent());
    }

    @Test
    void testUpdateArticleNotFound() {
        Long id = 1L;
        String title = "Updated Article";
        String content = "Updated Content";

        when(approvedNewsFeedRepository.findById(id)).thenReturn(Optional.empty());

        ResponseEntity<String> response = studentController.updateArticle(id, title, content, session);

        verify(approvedNewsFeedRepository, never()).save(any(ApprovedNewsFeed.class));
        assertEquals(ResponseEntity.notFound().build(), response);
    }

    @Test
    void testDeleteArticle() {
        Long id = 1L;

        ApprovedNewsFeed newsFeed = new ApprovedNewsFeed();
        newsFeed.setId(id);
        newsFeed.setTitle("Test Article");
        newsFeed.setContent("Test Content");

        when(approvedNewsFeedRepository.findById(id)).thenReturn(Optional.of(newsFeed));

        ResponseEntity<String> response = studentController.deleteArticle(id, session);

        verify(approvedNewsFeedRepository, times(1)).delete(newsFeed);
        assertEquals(ResponseEntity.ok("Article deleted successfully."), response);
    }

    @Test
    void testDeleteArticleNotFound() {
        Long id = 1L;

        when(approvedNewsFeedRepository.findById(id)).thenReturn(Optional.empty());

        ResponseEntity<String> response = studentController.deleteArticle(id, session);

        verify(approvedNewsFeedRepository, never()).delete(any(ApprovedNewsFeed.class));
        assertEquals(ResponseEntity.notFound().build(), response);
    }
}
