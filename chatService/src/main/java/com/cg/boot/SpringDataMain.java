package com.cg.boot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class SpringDataMain {

	  private static final Logger logger = LoggerFactory.getLogger(SpringDataMain.class);

	    public static void main(String[] args) {
	        SpringApplication.run(SpringDataMain.class, args);
	        logger.info("Spring data app running....");
	    }
		@Bean
		public CorsFilter corsFilter() {
			UrlBasedCorsConfigurationSource src = 
	                       new UrlBasedCorsConfigurationSource();
			CorsConfiguration configuration = new CorsConfiguration();
			configuration.setAllowCredentials(true);
			configuration.addAllowedHeader("*");
			configuration.addAllowedOrigin("*");
			configuration.addAllowedMethod("*");
			src.registerCorsConfiguration("/**", configuration);
			return new CorsFilter(src);		
		}

	}
