package com.cg.boot.chatservice;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.boot.chatservice.ApprovedRegistration;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    
    List<Message> findByRecipientIdOrderBySentAtDesc(Long recipientId);
    List<Message> findByRecipientId(Long recipientId);
	List<Message> findByGroupIdentifier(String groupId);
	void deleteByGroupIdentifier(String groupId);
	List<Message> findByRecipient(ApprovedRegistration loggedInUser);
	List<Message> findBySender(ApprovedRegistration loggedInUser);
    
}