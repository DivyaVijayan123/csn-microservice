package com.cg.boot.chatservice;

import java.time.LocalDateTime;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private ApprovedRegistrationRepository approvedRegistrationRepository;

    @Autowired
    private MessageRepository messageRepository;
    
    @Autowired
    private UserServiceClient userServiceClient;

    public long getUserIdFromUserService(HttpSession session) {
        //return userServiceClient.getUsername();
        ApprovedRegistration user = (ApprovedRegistration)session.getAttribute("loggedInUser");
        if (user != null) {
            return user.getId();
        } else {
            return 1; // or handle the case when user is not logged in
        }
    }

    private static final String LOGIN_MESSAGE = "loggedInUser";

    @PostMapping("/send")
    public ResponseEntity<?> sendMessage(@RequestBody MessageRequest messageRequest, HttpSession session) {
//        ApprovedRegistration sender = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);
    	long sender=1;
        Optional<ApprovedRegistration> recipientOptional = approvedRegistrationRepository
                .findById(messageRequest.getRecipientId());
        if (!recipientOptional.isPresent()) {
            return ResponseEntity.badRequest()
                    .body("Recipient with ID " + messageRequest.getRecipientId() + " does not exist or is not an approved user.");
        }
        ApprovedRegistration recipient = recipientOptional.get();

        Message message = new Message();
        message.setSender(sender);
        message.setRecipient(recipient);
        message.setContent(messageRequest.getContent());
        message.setSentAt(LocalDateTime.now());

        messageRepository.save(message);

        return ResponseEntity.ok("Message sent to user " + recipient.getFirstName() + " successfully!");
    }

    @GetMapping("/messages")
    public ResponseEntity<?> getMessagesForLoggedInUser(HttpSession session) {
        ApprovedRegistration loggedInUser = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);

        List<Message> sentMessages = messageRepository.findBySender(loggedInUser);
        List<Message> receivedMessages = messageRepository.findByRecipient(loggedInUser);

        List<Message> messages = new ArrayList<>();
        messages.addAll(sentMessages);
        messages.addAll(receivedMessages);

        return ResponseEntity.ok(messages);
    }

    @PostMapping("/send-group")
    public ResponseEntity<?> sendGroupMessage(@RequestBody GroupMessageRequest groupMessageRequest,
            HttpSession session) {
//        ApprovedRegistration sender = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);
    	long sender=1;
        List<Long> recipientIds = groupMessageRequest.getRecipientIds();
        if (recipientIds.isEmpty()) {
            return ResponseEntity.badRequest().body("Recipient IDs cannot be empty.");
        }

        List<ApprovedRegistration> recipients = approvedRegistrationRepository.findAllById(recipientIds);
        if (recipients.isEmpty()) {
            return ResponseEntity.badRequest()
                    .body("Recipients do not exist or are not approved users.");
        }

        LocalDateTime now = LocalDateTime.now();
        String group = groupMessageRequest.getGroupIdentifier();

        for (ApprovedRegistration recipient : recipients) {
            Message message = new Message();
            message.setSender(sender);
            message.setGroupIdentifier(group);
            message.setContent(groupMessageRequest.getContent());
            message.setSentAt(now);
            message.setRecipient(recipient);

            messageRepository.save(message);
        }

        return ResponseEntity.ok("Group message sent successfully!");
    }

    @PutMapping("/update/{messageId}")
    public ResponseEntity<?> updateMessage(@PathVariable Long messageId, @RequestBody MessageUpdate messageUpdate) {
        Optional<Message> messageOptional = messageRepository.findById(messageId);
        if (!messageOptional.isPresent()) {
            return ResponseEntity.badRequest().body("Message with ID " + messageId + " does not exist.");
        }

        Message message = messageOptional.get();
        message.setContent(messageUpdate.getContent());
        message.setUpdatedAt(LocalDateTime.now());

        messageRepository.save(message);

        return ResponseEntity.ok("Message updated successfully!");
    }

    @DeleteMapping("/delete/{messageId}")
    public ResponseEntity<?> deleteMessage(@PathVariable Long messageId) {
        Optional<Message> messageOptional = messageRepository.findById(messageId);
        if (!messageOptional.isPresent()) {
            return ResponseEntity.badRequest().body("Message with ID " + messageId + " does not exist.");
        }

        messageRepository.deleteById(messageId);

        return ResponseEntity.ok("Message deleted successfully!");
    }

    @PutMapping("/update-group/{groupId}")
    public ResponseEntity<?> updateGroupMessage(@PathVariable String groupId,
            @RequestBody MessageUpdate messageUpdate) {
        List<Message> groupMessages = messageRepository.findByGroupIdentifier(groupId);
        if (groupMessages.isEmpty()) {
            return ResponseEntity.badRequest()
                    .body("No messages found for group with identifier " + groupId + ".");
        }

        for (Message message : groupMessages) {
            message.setContent(messageUpdate.getContent());
            message.setUpdatedAt(LocalDateTime.now());
            messageRepository.save(message);
        }

        return ResponseEntity.ok("Group messages updated successfully!");
    }

    @DeleteMapping("/delete-group/{groupId}")
    public ResponseEntity<?> deleteGroupMessage(@PathVariable String groupId) {
        List<Message> groupMessages = messageRepository.findByGroupIdentifier(groupId);
        if (groupMessages.isEmpty()) {
            return ResponseEntity.badRequest()
                    .body("No messages found for group with identifier " + groupId + ".");
        }

        messageRepository.deleteAll(groupMessages);

        return ResponseEntity.ok("Group messages deleted successfully!");
    }

    @GetMapping("/group-messages")
    public ResponseEntity<?> getGroupMessages(@RequestParam String groupIdentifier) {
        List<Message> groupMessages = messageRepository.findByGroupIdentifier(groupIdentifier);
        return ResponseEntity.ok(groupMessages);
    }

    static class MessageRequest {
        private Long recipientId;
        private String content;

        public Long getRecipientId() {
            return recipientId;
        }

        public void setRecipientId(Long recipientId) {
            this.recipientId = recipientId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
