package com.cg.boot.chatservice;
public class MessageUpdate {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}