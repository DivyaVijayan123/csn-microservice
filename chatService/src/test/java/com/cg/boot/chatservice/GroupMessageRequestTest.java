package com.cg.boot.chatservice;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

class GroupMessageRequestTest {

    @Test
    void testGettersAndSetters() {
        // Create an instance of GroupMessageRequest
        GroupMessageRequest request = new GroupMessageRequest();

        // Set values using setters
        String groupIdentifier = "group-1";
        List<Long> recipientIds = new ArrayList<>();
        recipientIds.add(1L);
        recipientIds.add(2L);
        String content = "Test Content";

        request.setGroupIdentifier(groupIdentifier);
        request.setRecipientIds(recipientIds);
        request.setContent(content);

        // Verify the values using getters
        assertEquals(groupIdentifier, request.getGroupIdentifier());
        assertEquals(recipientIds, request.getRecipientIds());
        assertEquals(content, request.getContent());
    }
}
